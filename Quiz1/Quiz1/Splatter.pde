class Splatter
{
  float xPosition;
  float yPosition;
  
  float size;
  float volume;
  
  float step = 25;
  
  float xLimit = 960;
  float yLimit = 520;
  
  float count;
  
  
  Splatter()
  {
  }
  
  Splatter(float x, float y)
  {
    xPosition = x;
    yPosition = y;
  }
  
  void render()
  {
    float standardDeviation = 60;
    float mean = 200;
    float x = standardDeviation * randomGaussian() + mean;
    size = int(randomGaussian() * step);
    volume = int(randomGaussian());    
    for (int i = 0; i < volume; i++) {  
      float xPosition = random(-x, x);
      float yPosition = random(-x, x);
      circle (xPosition, yPosition, size);
      fill( random(255), random(255), random(255), random(255));
      noStroke();
      count++;
      println("frameCount: " + frameCount);
    }
  }
  
  void ResetCount(){
    if (count >= 100){
      count = 0; 
      background(230);
    }
    
    if (frameCount >= 100){
      frameCount = 0; 
    }
  }
}
