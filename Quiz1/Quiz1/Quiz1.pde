void setup() //gets called when the program runs
{
  size(1920, 1080,P3D);
  camera(0,0, -(height /2) / tan (PI * 30 / 180 ),
         0, 0, 0,
         0, -1, 0);
         background(230);
}
Splatter splatter = new Splatter();

void draw()
{
  splatter.render();
  splatter.ResetCount();
}
