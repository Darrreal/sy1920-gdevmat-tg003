class Walker
{
  float xPosition;
  float yPosition;
  float size;
  float n;
  float stepX;
  float stepY;

  float dt = 0;
  float dx = 10;
  float dy = 15;

  //rgb
  float r_t = 100;
  float g_t = 150;
  float b_t = 200;


  Walker()
  {
    xPosition = 0;
    yPosition = 0;
  }

  Walker (float x, float y)
  {
    xPosition = x;
    yPosition = y;
  }

  void render()
  {
    float n = noise(dt);
    dt += 0.01f;
    //
    circle (xPosition, yPosition, size);
    //pseudo for fill
    fill (map(noise(r_t), 0, 1, 0, 255), map(noise(g_t), 0, 1, 0, 255), map(noise(b_t), 0, 1, 0, 255), 255); 
    r_t += 0.1f;
    g_t += 0.1f;
    b_t += 0.1f;

    size = 15/n;


    float x = noise(dx);
    stepX = map(x, 0, 1, Window.left, Window.right);

    float y = noise(dy);
    stepY = map(y, 0, 1, Window.bottom, Window.top);

    dt += 0.01f;
    dx += 0.01f;
    dy += 0.01f;

    println("n: "+n);
    noStroke();
  }

  void walk()
  {
    xPosition = stepX;
    yPosition = stepY;
  }
}
