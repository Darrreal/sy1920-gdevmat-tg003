void setup() //gets called when the program runs
{
  size(1920, 1080,P3D);
  camera(0,0, -(height /2) / tan (PI * 30 / 180 ),
         0, 0, 0,
         0, -1, 0);
}

void draw() //gets called every frame
{
  background(130);
  drawCartesianPlane();
  drawLinearFunction();
  drawQuadraticFunction();
  drawCircle();
  makeWave();
  key();
}

void drawCartesianPlane()
{
    
  line(300, 0, -300, 0);
  line(0, 300, 0, -300);

  for(int i = -300; i <=300; i+=10)
  {
    line(i, -5, i, 5);
    line(-5, i, 5, i);
  }
}

void drawLinearFunction()
{
  /*
    f(x) = x + 2
    Let x be 4, then y = 6 (4, 6)
    Let x ve -5, then y = -3 (-5, 3)
  */
  
  for (int x = -200; x <= 200; x++)
  {
     circle(x, x+2, 1); 
  }
}

void drawQuadraticFunction()
{
  /*
    f(x) = x^2 + 2x - 5
    Let x be 2, then y = 3
    Let x be -2, then y = 3
    Let x be -1, then y = -6
  */
  
  for (float x = -300; x <= 300; x+=0.1)
  {
    circle(x * 10 , (x * x) + (2 * x) - 5, 1);
  }
}

float radius = 50;

void drawCircle()
{
  for (int x = 0; x < 360; x++)
  {
    circle((float) Math.cos(x) * radius, (float) Math.sin(x) * radius, 1);
  }
  //radius++;
}

float diameter = 16;
float t = 0;
float dt = 0.1;
float amplitude = 100;
float frequency = 0.5;
void makeWave()

{
  for (float x = -300; x <= 300; x+=.5)
  {
    ellipse (x*diameter, amplitude*sin (frequency*(t+x)), diameter*2, diameter*2);
    noStroke();
  }
  t += dt;
}

void key()
{
  if (keyPressed) 
  {
    if (keyCode == RIGHT) {
      diameter+=0.5;
    } else if (keyCode == LEFT) {
      diameter-=0.5;
    } 
    if (keyCode == UP) {
      amplitude+=1.5;
    } else if (keyCode == DOWN) {
      amplitude-=1.5;
    }     
  }
}
