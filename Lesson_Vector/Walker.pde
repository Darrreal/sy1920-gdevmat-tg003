public class Walker
{
  public Vector2 position;
  public float scale = 50;
  
  Walker()
  {
    position = new Vector2();
  }
  
  Walker(float x, float y)
  {
    position = new Vector2(x,y);
  }
  
  Walker(Vector2 position)
  {
    this.position = position;
  }
  
  public void render()
  {
    circle (position.x, position.y, scale);
  }
  
  public void randomWalk()
  {
    float decision = random(0,4);
    if (decision == 0)
    {
      position.x++;
    }
    else if (decision == 1)
    {
      position.x--;
    }
    else if (decision == 2)
    {
      position.y++;
    }
    else if (decision == 3)
    {
      position.y--;
    }
    
  }
}
