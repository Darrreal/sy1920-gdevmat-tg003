void setup() //gets called when the program runs
{
  size(1920, 1080,P3D);
  camera(0,0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
}

//Vector2 pos = new Vector2();
Walker walker = new Walker();
Vector2 vel = new Vector2(5,8);

Vector2 offset = new Vector2(100,100);

Vector2 mousePos()
{
  float x = mouseX - Window.windowWidth;
  float y = -(mouseY - Window.windowHeight);
  return new Vector2(x,y);
}

float r_t = 100;
  float g_t = 150;
  float b_t = 200;

void draw()
{
  background(0);
  
  
  Vector2 mouse = mousePos();
  mouse.normalize();
  mouse.mult(300);
    
  strokeWeight(10);
  stroke(map(noise(r_t),0,1,0,255),map(noise(g_t),0,1,0,255),map(noise(b_t),0,1,0,255),255);
  line (-mouse.x,-mouse.y,mouse.x,mouse.y);
    r_t += 0.1f;
    g_t += 0.1f;
    b_t += 0.1f;
  
  
  strokeWeight(10);
  stroke(map(noise(r_t),0,1,0,255),map(noise(g_t),0,1,0,255),map(noise(b_t),0,1,0,255),255);
  line (mouse.x,mouse.y,-mouse.x,-mouse.y);
    r_t += 0.1f;
    g_t += 0.1f;
    b_t += 0.1f;
    
  mouse.mult(.25);
  strokeWeight(15);
  stroke(150,30,0);
  line (mouse.x,mouse.y,-mouse.x,-mouse.y);
  
  
  
  /*mouse.mult(.5);
  strokeWeight(10);
  stroke(200,0,0);
  line (offset1.x,offset1.y,mouse.x+ offset1.x,mouse.y+offset1.y);*/
  
  
  
  /*circle(walker.position.x,walker.position.y,50);
  walker.position.x += vel.x;
  walker.position.y += vel.y;
  
  if (walker.position.x > Window.left || walker.position.x < Window.right)
  {
    vel.x *= -1;
  }
  
  if (walker.position.y > Window.top || walker.position.y < Window.bottom)
  {
    vel.y *= -1;
  }*/
}
