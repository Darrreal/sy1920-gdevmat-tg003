Mover mover;

void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  mover = new Mover();
  //mover.velocity = new PVector(10,25);
  mover.position.x = Window.left + 50;
  mover.acceleration = new PVector(0.1, 0);
  
}

void draw()
{
  background(255);
  
  fill(200,0,0);
  
  mover.render();
  
  if(mover.position.x > Window.right)
  {
    mover.position.x = Window.left + 50;
  }
  
  else if(mover.position.x >= mover.origPos.x)
  {
    mover.acceleration = new PVector(-0.1, 0);
  }
  
  if (mover.velocity.x <= 0)
  {
    mover.acceleration = new PVector(0, 0);
  }
}
