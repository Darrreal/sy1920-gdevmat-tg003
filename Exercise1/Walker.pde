class Walker
{
  float xPosition;
  float yPosition;
  int step = 30;
  
  float xLimit = 960;
  float yLimit = 520;
  
  Walker()
  {
    xPosition = 0;
    yPosition = 0;
  }
  
  Walker (float x, float y)
  {
    xPosition = x;
    yPosition = y;
  }
  
  void render()
  {
    circle (xPosition, yPosition, step);
    fill( random(255), random(255), random(255), random(255));
  }
  
  void walk()
  {
    int direction = floor(random(9));
    if (direction == 0)
    {
      yPosition+=step;
    }
    
    else if (direction == 1)
    {
      yPosition-=step;
    }
    
    else if (direction == 2)
    {
      xPosition+=step;
    }
    
    else if (direction == 3)
    {
      xPosition-=step;
    }
    
    else if (direction == 5)
    {
      yPosition+=step;
      xPosition+=step;
    }
    
    else if (direction == 6)
    {
      yPosition+=step;
      xPosition-=step;
    }
    
    else if (direction == 7)
    {
      yPosition-=step;
      xPosition+=step;
    }
    
    else if (direction == 8)
    {
      yPosition-=step;
      xPosition-=step;
    }
  }
  
  void boundary()
  {
    println("xPosition: "+yPosition);
    if (yPosition > yLimit)
    {
      yPosition = (-yLimit+step);
    }
    
    if (yPosition < -yLimit)
    {
      yPosition = (yLimit-step);
    }
    
    if (xPosition > xLimit)
    {
      xPosition = (-xLimit+step);
    }
    
    if (xPosition < -xLimit)
    {
      xPosition = (xLimit-step);
    }
  }
}
